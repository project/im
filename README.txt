This module lets registered users send messages to each other.
The messages will be displayed at the top of each page.

The Instant Messages are sent via a messaging block that allows you to
select the user and then send a message to that user. The message
appears at the top of the next page viewed by that user.

